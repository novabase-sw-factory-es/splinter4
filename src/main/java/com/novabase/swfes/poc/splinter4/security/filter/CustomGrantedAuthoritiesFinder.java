package com.novabase.swfes.poc.splinter4.security.filter;

import java.util.Arrays;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public class CustomGrantedAuthoritiesFinder {

  public List<GrantedAuthority> findGrantedAuthorities(final String id) {
    String lastChar = id.substring(id.length() - 1);
    try {
      int intValue = Integer.valueOf(lastChar);
      return intValue % 2 == 0
        ? Arrays.asList(new SimpleGrantedAuthority("ROLE_REGISTERED_USER"), new SimpleGrantedAuthority("ROLE_ADMINISTRATOR"))
        : Arrays.asList(new SimpleGrantedAuthority("ROLE_REGISTERED_USER"));
    } catch (NumberFormatException e) {
      return Arrays.asList();
    }
  }

}
