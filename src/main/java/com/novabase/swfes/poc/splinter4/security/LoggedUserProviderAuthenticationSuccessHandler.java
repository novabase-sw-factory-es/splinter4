package com.novabase.swfes.poc.splinter4.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

@Slf4j
public class LoggedUserProviderAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

  private ObjectMapper objectMapper = new ObjectMapper();

  @Override
  public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
    LoggedUser loggedUser = LoggedUser.fromAuthentication(authentication);
    log.debug("Authenticated {}", loggedUser);
    String loggedUserJson = objectMapper.writeValueAsString(loggedUser);
    try (OutputStream os = response.getOutputStream()) {
      os.write(loggedUserJson.getBytes());
    }
  }
}
