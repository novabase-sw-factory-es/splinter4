package custom.waffle.servlet;

import com.novabase.swfes.poc.splinter4.security.filter.CustomGrantedAuthoritiesFinder;
import com.sun.jna.platform.win32.Win32Exception;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import waffle.servlet.WindowsPrincipal;
import waffle.spring.GuestLoginDisabledAuthenticationException;
import waffle.spring.WindowsAuthenticationProvider;
import waffle.spring.WindowsAuthenticationToken;
import waffle.windows.auth.IWindowsIdentity;

@Slf4j
public class CustomWindowsAuthenticationProvider extends WindowsAuthenticationProvider {

  private CustomGrantedAuthoritiesFinder customGrantedAuthoritiesFinder;

  public CustomWindowsAuthenticationProvider(CustomGrantedAuthoritiesFinder customGrantedAuthoritiesFinder) {
    super();
    this.customGrantedAuthoritiesFinder = customGrantedAuthoritiesFinder;
  }

  @Override
  public Authentication authenticate(final Authentication authentication) {
    final UsernamePasswordAuthenticationToken auth = (UsernamePasswordAuthenticationToken) authentication;
    IWindowsIdentity windowsIdentity;
    try {
      windowsIdentity = this.getAuthProvider().logonUser(auth.getName(), auth.getCredentials().toString());
    } catch (final Win32Exception e) {
      throw new AuthenticationServiceException(e.getMessage(), e);
    }
    log.debug("logged in user: {} ({})", windowsIdentity.getFqn(),
      windowsIdentity.getSidString());

    if (!this.isAllowGuestLogin() && windowsIdentity.isGuest()) {
      log.warn("guest login disabled: {}", windowsIdentity.getFqn());
      throw new GuestLoginDisabledAuthenticationException(windowsIdentity.getFqn());
    }

    final WindowsPrincipal windowsPrincipal = new WindowsPrincipal(windowsIdentity, this.getPrincipalFormat(),
      this.getRoleFormat());
    log.debug("roles: {}", windowsPrincipal.getRolesString());

    final WindowsAuthenticationToken token = new CustomWindowsAuthenticationToken(windowsPrincipal,
      this.getGrantedAuthorityFactory(), this.getDefaultGrantedAuthority(), customGrantedAuthoritiesFinder);

    log.info("successfully logged in user: {}", windowsIdentity.getFqn());
    return token;
  }

}
