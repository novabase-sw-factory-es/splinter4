package custom.waffle.servlet;

import com.novabase.swfes.poc.splinter4.security.filter.CustomGrantedAuthoritiesFinder;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import waffle.servlet.AutoDisposableWindowsPrincipal;
import waffle.servlet.WindowsPrincipal;
import waffle.spring.NegotiateSecurityFilter;
import waffle.util.AuthorizationHeader;
import waffle.windows.auth.IWindowsIdentity;
import waffle.windows.auth.IWindowsImpersonationContext;

@Slf4j
public class CustomNegotiateSecurityFilter extends NegotiateSecurityFilter {

  private CustomGrantedAuthoritiesFinder customGrantedAuthoritiesFinder;

  public CustomNegotiateSecurityFilter(CustomGrantedAuthoritiesFinder customGrantedAuthoritiesFinder) {
    super();
    this.customGrantedAuthoritiesFinder = customGrantedAuthoritiesFinder;
  }

  @Override
  public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
    throws IOException, ServletException {

    final HttpServletRequest request = (HttpServletRequest) req;
    final HttpServletResponse response = (HttpServletResponse) res;

    log.debug("{} {}, contentlength: {}", request.getMethod(), request.getRequestURI(),
      Integer.valueOf(request.getContentLength()));

    final AuthorizationHeader authorizationHeader = new AuthorizationHeader(request);

    // authenticate user
    if (!authorizationHeader.isNull()
      && this.getProvider().isSecurityPackageSupported(authorizationHeader.getSecurityPackage())) {

      // log the user in using the token
      IWindowsIdentity windowsIdentity;

      try {
        windowsIdentity = this.getProvider().doFilter(request, response);
        if (windowsIdentity == null) {
          return;
        }
      } catch (final IOException e) {
        log.warn("error logging in user: {}", e.getMessage());
        log.trace("", e);
        this.sendUnauthorized(response, true);
        return;
      }

      if (!this.isAllowGuestLogin() && windowsIdentity.isGuest()) {
        log.warn("guest login disabled: {}", windowsIdentity.getFqn());
        this.sendUnauthorized(response, true);
        return;
      }

      IWindowsImpersonationContext ctx = null;
      try {
        log.debug("logged in user: {} ({})", windowsIdentity.getFqn(),
          windowsIdentity.getSidString());

        final WindowsPrincipal principal = this.isImpersonate()
          ? new AutoDisposableWindowsPrincipal(windowsIdentity, this.getPrincipalFormat(), this.getRoleFormat())
          : new WindowsPrincipal(windowsIdentity, this.getPrincipalFormat(), this.getRoleFormat());

        log.debug("roles: {}", principal.getRolesString());

        final Authentication authentication = new CustomWindowsAuthenticationToken(principal,
          this.getGrantedAuthorityFactory(), this.getDefaultGrantedAuthority(), customGrantedAuthoritiesFinder);

        if (!this.setAuthentication(request, response, authentication)) {
          return;
        }

        log.info("successfully logged in user: {}", windowsIdentity.getFqn());

        if (this.isImpersonate()) {
          log.debug("impersonating user");
          ctx = windowsIdentity.impersonate();
        }

        chain.doFilter(request, response);
      } finally {
        if (this.isImpersonate() && ctx != null) {
          log.debug("terminating impersonation");
          ctx.revertToSelf();
        } else {
          windowsIdentity.dispose();
        }
      }
    } else {
      chain.doFilter(request, response);
    }
  }

}
