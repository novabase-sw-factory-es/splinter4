package custom.waffle.servlet;

import com.novabase.swfes.poc.splinter4.security.filter.CustomGrantedAuthoritiesFinder;
import org.springframework.security.core.GrantedAuthority;
import waffle.servlet.WindowsPrincipal;
import waffle.spring.GrantedAuthorityFactory;
import waffle.spring.WindowsAuthenticationToken;

public class CustomWindowsAuthenticationToken extends WindowsAuthenticationToken {

  public CustomWindowsAuthenticationToken(final WindowsPrincipal identity,
    final GrantedAuthorityFactory grantedAuthorityFactory, final GrantedAuthority defaultGrantedAuthority,
    final CustomGrantedAuthoritiesFinder customGrantedAuthoritiesFinder) {

    super(identity, grantedAuthorityFactory, defaultGrantedAuthority);
    this.getAuthorities().clear();
    this.getAuthorities().addAll(customGrantedAuthoritiesFinder.findGrantedAuthorities(identity.getName()));
  }

}
