/**
 * This package overrides the default behaviour of NegotiateSecurityFilter class from package waffle.servlet

 * The main differences between original classes are
      * CustomNegotiateSecurityFilter extends original one and overrides only doFilter method.
          It is created with CustomGrantedAuthoritiesFinder class that implements logic to add more roles to logged user.
          doFilter method, in lines 78 and 79, creates Authentication with CustomWindowsAuthenticationToken class.
      * CustomWindowsAuthenticationProvider extends original one and overrides only authenticate method.
         It is created with CustomGrantedAuthoritiesFinder class that implements logic to add more roles to logged user.
         authenticate method, in lines 46 and 47, creates WindowsAuthenticationToken with CustomWindowsAuthenticationToken class.
      * CustomWindowsAuthenticationToken overrides WindowsAuthenticationToken.
          In constructor, calls parent constructor, cleans authorities and adds custom roles by id from CustomGrantedAuthoritiesFinder
          If all roles are going to be used, remove line 16 from CustomWindowsAuthenticationToken.
 */

package custom.waffle.servlet;
