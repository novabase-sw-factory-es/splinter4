package com.novabase.swfes.poc.splinter4.integration.unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.novabase.swfes.poc.splinter4.security.filter.CustomGrantedAuthoritiesFinder;
import java.util.List;
import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public class CustomGrantedAuthoritiesFinderUnitTest {

  private final CustomGrantedAuthoritiesFinder customGrantedAuthoritiesFinder = new CustomGrantedAuthoritiesFinder();

  private final static String ADMIN_ID = "someId2";
  private final static String REGISTERED_USER_ID = "someId1";
  private final static String VISITOR_ID = "someId";

  private final static GrantedAuthority ADMIN_AUTHORITY = new SimpleGrantedAuthority("ROLE_ADMINISTRATOR");
  private final static GrantedAuthority USER_AUTHORITY = new SimpleGrantedAuthority("ROLE_REGISTERED_USER");

  @Test
  public void testAdminRole() {
    List<GrantedAuthority> adminAccountList = customGrantedAuthoritiesFinder.findGrantedAuthorities(ADMIN_ID);

    assertEquals(2, adminAccountList.size());
    assertTrue(adminAccountList.contains(ADMIN_AUTHORITY));
    assertTrue(adminAccountList.contains(USER_AUTHORITY));
  }

  @Test
  public void testRegisteredUserRole() {
    List<GrantedAuthority> registeredUserAccountList = customGrantedAuthoritiesFinder
      .findGrantedAuthorities(REGISTERED_USER_ID);

    assertEquals(1, registeredUserAccountList.size());
    assertEquals(USER_AUTHORITY, registeredUserAccountList.get(0));
  }

  @Test
  public void testVisitorId() {
    List<GrantedAuthority> visitorAccountList = customGrantedAuthoritiesFinder.findGrantedAuthorities(VISITOR_ID);

    assertTrue(visitorAccountList.isEmpty());
  }

}
