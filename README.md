Sample application with Spring Boot 2 and Waffle for MS domain authentication.

Configured for Spring Form authentication and Waffle Negotiate in only one URL. Frontend should use Negotiate on such URL and fallback to form if unable.

Boot and browse http://localhost:8080/

By default, it allows ROLE_USER users (which is added by default by Waffle).

Login is available with user and password credentials or with Windows authentication token. With both of them, new custom roles will replace original ones depending on users id. See package-info in custom.waffle.servlet package for more information.

Some Waffle classes have been overwritten to allow this behaviour. In CustomGrantedAuthoritiesFinder class is where final logic must be implemented (for example, loading roles from a DDBB).
